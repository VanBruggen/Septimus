# Septimus

A multiplatform todo list on steroids. Written in Flutter, dedicated for Android, Windows and Linux.

## Downloads

- [Android (.apk)](https://pad.artemislena.eu/file/#/2/file/kR62xfgvj7udfbS+mpvpb2i5/)

- [Windows (.exe)](https://pad.artemislena.eu/file/#/2/file/ykry9of+FSHyw4ZFFnO7aAyo/)

- [Linux (.appimage)](https://pad.artemislena.eu/file/#/2/file/u-yu5Jfo7Q3phAxecw9yhkv9/)

## Screenshots

Annotations are currently only in polish.

### Root

<img src="https://codeberg.org/VanBruggen/Septimus/media/branch/main/screenshots/ui-root.png" width="70%">

### Task

<img src="https://codeberg.org/VanBruggen/Septimus/media/branch/main/screenshots/ui-task.png" width="80%">

### Task details

<img src="https://codeberg.org/VanBruggen/Septimus/media/branch/main/screenshots/ui-task-details.png" width="90%">

### Edit task

<img src="https://codeberg.org/VanBruggen/Septimus/media/branch/main/screenshots/ui-edit-task.png" width="80%">

<img src="https://codeberg.org/VanBruggen/Septimus/media/branch/main/screenshots/ui-datetime.png" width="80%">

### Sort modes

<img src="https://codeberg.org/VanBruggen/Septimus/media/branch/main/screenshots/ui-sort-modes.jpg" width="30%">

### Task actions

<img src="https://codeberg.org/VanBruggen/Septimus/media/branch/main/screenshots/ui-task-actions.jpg" width="30%">

### Move/copy task(s)

<img src="https://codeberg.org/VanBruggen/Septimus/media/branch/main/screenshots/ui-move-copy.png" width="90%">

## Authors

- Szymon Wiśniewski

- Jakub Więcek

- Piotr Zajdziński

## Acknowledgment

- Creators of [GetX](https://pub.dev/packages/get) and [GetStorage](https://pub.dev/packages/get_storage)

- Creators of [Flutter Form Builder](https://pub.dev/packages/flutter_form_builder)

- Creators of [Dart](https://dart.dev) & [Flutter](https://flutter.dev)