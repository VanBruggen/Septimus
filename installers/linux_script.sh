#!/bin/sh
# build the project
flutter build linux --release
# copy the bundle folder to AppDir
cp -r ../build/linux/release/x64/bundle/ Septimus.AppDir
# copy icon to AppDir folder.
cp logo.png Septimus.AppDir/
# Build AppRun file
echo "#!/bin/sh" > AppRun
echo 'cd "$(dirname "$0")"' >> AppRun
echo 'exec ./Septimus' >> AppRun
chmod +x AppRun
cp AppRun Septimus.AppDir/
# Build .desktop file
echo '[Desktop Entry]' > Septimus.desktop
echo 'Version=1.0' >> Septimus.desktop
echo 'Type=Application' >> Septimus.desktop
echo 'Terminal=false' >> Septimus.desktop
echo 'Name=Septimus' >> Septimus.desktop
echo 'Exec=septimus %u' >> Septimus.desktop
echo 'Icon=logo' >> Septimus.desktop
echo 'Categories=Utility;' >> Septimus.desktop
cp Septimus.desktop Septimus.AppDir/
# download appimagetool
wget "https://github.com/AppImage/AppImageKit/releases/download/continuous/appimagetool-x86_64.AppImage"
chmod a+x appimagetool-x86_64.AppImage
# Build your .AppImage app
./appimagetool-x86_64.AppImage Septimus.AppDir/ 