import 'package:get/get.dart';
import 'package:septimus/task/controllers/task_explorer.controller.dart';
import 'package:septimus/task/task_storage.service.dart';

Future<void> init() async {
  await Get.putAsync<TaskStorage>(() => TaskStorage.construct(),
      permanent: true);
  Get.put<TaskExplorerController>(TaskExplorerController(), permanent: true);
}
