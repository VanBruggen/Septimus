// TODO: Before any operation on a subtask:
// TODO: 1. Ensure subtasksIds is not null nor empty
// TODO: 2. Ensure that the given Task's id is inside of currentTask.subtasksIds

import 'package:get/get.dart';
import 'package:septimus/task/task.model.dart';
import 'package:septimus/task/task_storage.service.dart';

extension Subtasks on Task {
  List<Task>? get subtasks =>
      subtasksIds?.map((id) => Get.find<TaskStorage>().load(id: id)).toList();
}

class TaskExplorerController extends GetxController {
  late Task currentTask;
  bool isDetailsSectionExpanded = false;

  TaskStorage taskStorage = Get.find();

  @override
  onInit() {
    super.onInit();

    if (taskStorage.getLastOpenTaskId() != null) {
      currentTask = taskStorage.load(id: taskStorage.getLastOpenTaskId()!);
    } else if (taskStorage.contains(id: 0)) {
      currentTask = taskStorage.load(id: 0);
    } else {
      taskStorage.clear();
      currentTask = Task.root();
      taskStorage.save(currentTask);
    }
  }

  void navigateTo({required int id}) {
    if (taskStorage.contains(id: id)) {
      _sortSubtasksOf(currentTask);
      taskStorage.save(currentTask);
      currentTask = taskStorage.load(id: id);
      taskStorage.setLastOpenTaskId(currentTask.id);
      update();
    } else {
      print('Error: TaskExplorerController'
          ' => navigateTo($id)'
          ' => W bazie nie ma Taska o id \'$id\'!');
    }
  }

  void navigateUp() {
    navigateTo(id: currentTask.parentId);
  }

  void navigateToRoot() {
    navigateTo(id: 0);
  }

  void setCurrentTaskSortMode(SubtasksSortMode sortMode) {
    currentTask.subtasksSortMode = sortMode;
    _sortSubtasksOf(currentTask);
    taskStorage.save(currentTask);
    update();
  }

  void sort() {
    _sortSubtasksOf(currentTask);
    taskStorage.save(currentTask);
    update();
  }

  void checkAllSubtasks() {
    if (currentTask.subtasksIds != null) {
      for (var subtask in currentTask.subtasks!) {
        subtask.isChecked = true;
        taskStorage.save(subtask);
      }
      update();
    }
  }

  void uncheckAllSubtasks() {
    if (currentTask.subtasksIds != null) {
      for (var subtask in currentTask.subtasks!) {
        subtask.isChecked = false;
        taskStorage.save(subtask);
      }
      update();
    }
  }

  void newSubtask({required String newTaskName}) {
    final newTask = Task(
        id: taskStorage.newTaskId(),
        parentId: currentTask.id,
        name: newTaskName);

    _addSubtask(to: currentTask, taskToAdd: newTask);

    taskStorage.save(newTask);
    taskStorage.save(currentTask);
    update();
  }

  void toggleSubtaskCheck(Task passedTask) {
    // var passedTask = subtasks!.singleWhere((_) => _.id == passedId);

    if (passedTask.isChecked) {
      passedTask.isChecked = false;
    } else {
      passedTask.isChecked = true;
    }

    taskStorage.save(passedTask);
    update();
  }

  void cycleSubtaskPriority(Task passedTask) {
    switch (passedTask.priority) {
      case Priority.veryLow:
        passedTask.priority = Priority.low;
        break;
      case Priority.low:
        passedTask.priority = Priority.normal;
        break;
      case Priority.normal:
        passedTask.priority = Priority.high;
        break;
      case Priority.high:
        passedTask.priority = Priority.veryHigh;
        break;
      case Priority.veryHigh:
        passedTask.priority = Priority.veryLow;
        break;
      default:
        break;
    }

    taskStorage.save(passedTask);
    update();
  }

  void deleteSubtask(Task passedTask) {
    _deleteSubtaskTree(passedTask);
    currentTask.subtasksIds!.removeWhere((id) => id == passedTask.id);

    if (currentTask.subtasksIds!.isEmpty) currentTask.subtasksIds = null;
    taskStorage.save(currentTask);
    update();
  }

  void deleteCheckedSubtasks() {
    if (currentTask.subtasksIds != null) {
      for (var checkedSubtask
          in (currentTask.subtasks!.where((subtask) => subtask.isChecked))) {
        _deleteSubtaskTree(checkedSubtask);
        currentTask.subtasksIds!.removeWhere((id) => id == checkedSubtask.id);
      }

      if (currentTask.subtasksIds!.isEmpty) currentTask.subtasksIds = null;
      taskStorage.save(currentTask);
      update();
    }
  }

  void moveSubtask({required Task to, required Task taskToMove}) {
    _moveSubtask(to: to, taskToMove: taskToMove);

    _sortSubtasksOf(to);
    taskStorage.save(to);

    taskStorage.save(currentTask);
    update();
  }

  void moveCheckedSubtasks({required Task to}) {
    if (currentTask.subtasksIds != null) {
      for (var checkedSubtask
          in (currentTask.subtasks!.where((subtask) => subtask.isChecked))) {
        _moveSubtask(to: to, taskToMove: checkedSubtask);
      }

      _sortSubtasksOf(to);
      taskStorage.save(to);

      taskStorage.save(currentTask);
      update();
    }
  }

  void copySubtask({required Task to, required Task taskToCopy}) {
    _copySubtaskTree(to: to, taskToCopy: taskToCopy);
    update();
  }

  void copyCheckedSubtasks({required Task to}) {
    if (currentTask.subtasksIds != null) {
      for (var checkedSubtask
          in (currentTask.subtasks!.where((subtask) => subtask.isChecked))) {
        _copySubtaskTree(to: to, taskToCopy: checkedSubtask);
      }

      update();
    }
  }

  void _addSubtask({required Task to, required Task taskToAdd}) {
    if (to.subtasksIds == null) {
      to.subtasksIds = [taskToAdd.id];
    } else {
      to.subtasksIds!.add(taskToAdd.id);
    }
  }

  void _deleteSubtaskTree(Task passedTask) {
    if (passedTask.subtasksIds == null) {
      taskStorage.delete(passedTask);
    } else {
      for (var subtask in passedTask.subtasks!) {
        _deleteSubtaskTree(subtask);
      }

      taskStorage.delete(passedTask);
    }
  }

  void _moveSubtask({required Task to, required Task taskToMove}) {
    if (taskToMove.isChecked) taskToMove.isChecked = false;
    _addSubtask(to: to, taskToAdd: taskToMove);
    taskToMove.parentId = to.id;
    taskStorage.save(taskToMove);

    currentTask.subtasksIds!.removeWhere((id) => id == taskToMove.id);
    if (currentTask.subtasksIds!.isEmpty) currentTask.subtasksIds = null;
  }

  void _copySubtaskTree({required Task to, required Task taskToCopy}) {
    var copiedTask = taskToCopy.copyWith(
      id: taskStorage.newTaskId(),
      parentId: to.id,
      isChecked: false,
    );

    _addSubtask(
      to: to,
      taskToAdd: copiedTask,
    );

    taskStorage.save(copiedTask);
    _sortSubtasksOf(to);
    taskStorage.save(to);

    if (taskToCopy.subtasksIds != null) {
      for (var subtask in taskToCopy.subtasks!) {
        _copySubtaskTree(to: copiedTask, taskToCopy: subtask);
      }
    }
  }

  void showCurrentTaskDetails() {
    isDetailsSectionExpanded = true;
    update();
  }

  void hideCurrentTaskDetails() {
    isDetailsSectionExpanded = false;
    update();
  }

  void setDetails(
      {required Task target,
      required String newName,
      required Priority newPriority,
      required DateTime? newDeadline,
      required String? newDescription,
      required String? newPlace}) {
    target.name = newName;
    target.priority = newPriority;
    target.deadline = newDeadline;
    target.description = newDescription;
    target.place = newPlace;

    taskStorage.save(target);
    update();
  }

  void _sortSubtasksOf(Task task) {
    if (task.subtasksIds != null) {
      switch (task.subtasksSortMode) {
        case SubtasksSortMode.byPriority:
          _sortSubtasksByPriority(task);
          break;
        case SubtasksSortMode.byDeadline:
          _sortSubtasksByDeadline(task);
          break;
        case SubtasksSortMode.byName:
          _sortSubtasksByName(task);
          break;
        case SubtasksSortMode.byPlace:
          _sortSubtasksByPlace(task);
          break;
        default:
          break;
      }
    }
  }

  void _sortSubtasksByPriority(Task task) {
    var temp = task.subtasks!;
    temp.sort(
      (task1, task2) {
        final sortByCheckStatus = _sortTasksByCheckStatus(task1, task2);
        if (sortByCheckStatus == 0) {
          final sortByPriority = _sortTasksByPriority(task1, task2);
          if (sortByPriority == 0) {
            final sortByDeadline = _sortTasksByDeadline(task1, task2);
            if (sortByDeadline == 0) {
              final sortByName = _sortTasksByName(task1, task2);
              if (sortByName == 0) {
                return _sortTasksByPlace(task1, task2);
              }
              return sortByName;
            }
            return sortByDeadline;
          }
          return sortByPriority;
        }
        return sortByCheckStatus;
      },
    );
    task.subtasksIds = temp.map((subtask) => subtask.id).toList();
    taskStorage.save(task);
  }

  void _sortSubtasksByDeadline(Task task) {
    var temp = task.subtasks!;
    temp.sort(
      (task1, task2) {
        final int sortByCheckStatus = _sortTasksByCheckStatus(task1, task2);
        if (sortByCheckStatus == 0) {
          final int sortByDeadline = _sortTasksByDeadline(task1, task2);
          if (sortByDeadline == 0) {
            final sortByPriority = _sortTasksByPriority(task1, task2);
            if (sortByPriority == 0) {
              final sortByName = _sortTasksByName(task1, task2);
              if (sortByName == 0) {
                return _sortTasksByPlace(task1, task2);
              }
              return sortByName;
            }
            return sortByPriority;
          }
          return sortByDeadline;
        }
        return sortByCheckStatus;
      },
    );
    task.subtasksIds = temp.map((subtask) => subtask.id).toList();
    taskStorage.save(task);
  }

  void _sortSubtasksByName(Task task) {
    var temp = task.subtasks!;
    temp.sort(
      (task1, task2) {
        final int sortByCheckStatus = _sortTasksByCheckStatus(task1, task2);
        if (sortByCheckStatus == 0) {
          final int sortByName = _sortTasksByName(task1, task2);
          if (sortByName == 0) {
            final sortByPriority = _sortTasksByPriority(task1, task2);
            if (sortByPriority == 0) {
              final sortByDeadline = _sortTasksByDeadline(task1, task2);
              if (sortByDeadline == 0) {
                return _sortTasksByPlace(task1, task2);
              }
              return sortByDeadline;
            }
            return sortByPriority;
          }
          return sortByName;
        }
        return sortByCheckStatus;
      },
    );
    task.subtasksIds = temp.map((subtask) => subtask.id).toList();
    taskStorage.save(task);
  }

  void _sortSubtasksByPlace(Task task) {
    var temp = task.subtasks!;
    temp.sort(
      (task1, task2) {
        final int sortByCheckStatus = _sortTasksByCheckStatus(task1, task2);
        if (sortByCheckStatus == 0) {
          final int sortByPlace = _sortTasksByPlace(task1, task2);
          if (sortByPlace == 0) {
            final sortByPriority = _sortTasksByPriority(task1, task2);
            if (sortByPriority == 0) {
              final sortByDeadline = _sortTasksByDeadline(task1, task2);
              if (sortByDeadline == 0) {
                return _sortTasksByName(task1, task2);
              }
              return sortByDeadline;
            }
            return sortByPriority;
          }
          return sortByPlace;
        }
        return sortByCheckStatus;
      },
    );
    task.subtasksIds = temp.map((subtask) => subtask.id).toList();
    taskStorage.save(task);
  }

  int _sortTasksByCheckStatus(Task task1, Task task2) =>
      ((task1.isChecked && task2.isChecked) ||
              (!task1.isChecked && !task2.isChecked)
          ? 0
          : (task1.isChecked && !task2.isChecked ? 1 : -1));

  int _sortTasksByPriority(Task task1, Task task2) =>
      task1.priority.index.compareTo(task2.priority.index);

  int _sortTasksByDeadline(Task task1, Task task2) {
    if (task1.deadline == null && task2.deadline == null) {
      return 0;
    } else if (task1.deadline == null && task2.deadline != null) {
      return 1;
    } else if (task1.deadline != null && task2.deadline == null) {
      return -1;
    } else {
      return task1.deadline!.compareTo(task2.deadline!);
    }
  }

  int _sortTasksByName(Task task1, Task task2) =>
      task1.name.toLowerCase().compareTo(task2.name.toLowerCase());

  int _sortTasksByPlace(Task task1, Task task2) {
    if (task1.place == null && task2.place == null) {
      return 0;
    } else if (task1.place == null && task2.place != null) {
      return 1;
    } else if (task1.place != null && task2.place == null) {
      return -1;
    } else {
      return task1.place!.toLowerCase().compareTo(task2.place!.toLowerCase());
    }
  }
}
