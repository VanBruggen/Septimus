import 'package:get/get.dart';
import 'package:septimus/task/controllers/task_explorer.controller.dart';
import 'package:septimus/task/task.model.dart';
import 'package:septimus/task/task_storage.service.dart';

class TaskMoveCopyController extends GetxController {
  late Task currentTask;

  final TaskStorage taskStorage = Get.find();

  @override
  onInit() {
    super.onInit();

    currentTask = Get.find<TaskExplorerController>().currentTask;
  }

  void navigateTo({required int id}) {
    if (taskStorage.contains(id: id)) {
      currentTask = taskStorage.load(id: id);
      update();
    } else {
      print('Error: TaskMoveCopyController'
          ' => navigateTo($id)'
          ' => W bazie nie ma Taska o id \'$id\'!');
    }
  }

  void navigateUp() {
    navigateTo(id: currentTask.parentId);
  }

  void navigateToRoot() {
    navigateTo(id: 0);
  }
}
