enum Priority {
  veryHigh,
  high,
  normal,
  low,
  veryLow;

  @override
  String toString() {
    switch (this) {
      case veryHigh:
        return 'Very high';
      case high:
        return 'High';
      case normal:
        return 'Normal';
      case low:
        return 'Low';
      case veryLow:
        return 'Very low';
    }
  }
}

enum SubtasksSortMode { byPriority, byDeadline, byName, byPlace }

class Task {
  final int id;
  int parentId;
  String name;
  bool isChecked;
  Priority priority;
  SubtasksSortMode subtasksSortMode;
  List<int>? subtasksIds;
  DateTime? deadline;
  String? description;
  String? place;

  Task({
    required this.id,
    required this.parentId,
    required this.name,
    this.isChecked = false,
    this.priority = Priority.normal,
    this.subtasksSortMode = SubtasksSortMode.byPriority,
    this.subtasksIds,
    this.deadline,
    this.description,
    this.place,
  });

  Task.root() : this(id: 0, parentId: 0, name: '');

  Task copyWith({
    int? id,
    int? parentId,
    String? name,
    bool? isChecked,
    Priority? priority,
    SubtasksSortMode? subtasksSortMode,
    List<int>? subtasksIds,
    DateTime? deadline,
    String? description,
    String? place,
  }) {
    return Task(
      id: id ?? this.id,
      parentId: parentId ?? this.parentId,
      name: name ?? this.name,
      isChecked: isChecked ?? this.isChecked,
      priority: priority ?? this.priority,
      subtasksSortMode: subtasksSortMode ?? this.subtasksSortMode,
      subtasksIds: subtasksIds ?? this.subtasksIds,
      deadline: deadline ?? this.deadline,
      description: description ?? this.description,
      place: place ?? this.place,
    );
  }

  factory Task.fromJson(Map<String, dynamic> json) {
    return Task(
      id: int.parse(json['id']),
      parentId: int.parse(json['parentId']),
      name: json['name'],
      isChecked: json['isChecked'] == 'true' ? true : false,
      priority: json['priority'] != null
          ? Priority.values.byName(json['priority'])
          : Priority.normal,
      subtasksSortMode: json['subtasksSortMode'] != null
          ? SubtasksSortMode.values.byName(json['subtasksSortMode'])
          : SubtasksSortMode.byPriority,
      subtasksIds: json['subtasksIds']?.cast<int>(),
      deadline:
          json['deadline'] != null ? DateTime.parse(json['deadline']) : null,
      description: json['description'],
      place: json['place'],
    );
  }

  Map<String, dynamic> toJson() => {
        'id': id.toString(),
        'parentId': parentId.toString(),
        'name': name,
        'isChecked': isChecked.toString(),
        'priority': priority.name,
        'subtasksSortMode': subtasksSortMode.name,
        'subtasksIds': subtasksIds,
        'deadline': deadline?.toString(),
        'description': description,
        'place': place,
      };

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other is Task && runtimeType == other.runtimeType && id == other.id);
  }

  @override
  int get hashCode {
    return Object.hash(
      runtimeType,
      id,
    );
  }

  @override
  String toString() {
    return 'Task('
        'id: ${id.toString()}, '
        'parentId: ${parentId.toString()}, '
        'name: \'$name\', '
        'isChecked: ${isChecked.toString()},'
        'priority: ${priority.name}, '
        'subtasksSortMode: ${subtasksSortMode.name}, '
        'subtasksIds: ${subtasksIds.toString()},'
        'deadline: ${deadline.toString()}, '
        'description: \'$description\', '
        'place: $place'
        ')';
  }
}
