import 'package:get_storage/get_storage.dart';
import 'package:septimus/task/task.model.dart';

class TaskStorage {
  GetStorage storage;

  TaskStorage(this.storage);

  static Future<TaskStorage> construct() async {
    await GetStorage.init('tasks');
    return TaskStorage(GetStorage('tasks'));
  }

  bool contains({required int id}) => storage.hasData(id.toString());

  Task load({required int id}) {
    return Task.fromJson(storage.read(id.toString()));
  }

  void save(Task task) {
    storage.write(task.id.toString(), task.toJson());
  }

  void delete(Task task) {
    storage.remove(task.id.toString());
  }

  int newTaskId() {
    var temp = storage.hasData('newTaskId') ? storage.read('newTaskId') : 1;
    storage.write('newTaskId', temp + 1);
    return temp;
  }

  int? getLastOpenTaskId() => storage.read('lastOpenTaskId');

  void setLastOpenTaskId(int id) {
    storage.write('lastOpenTaskId', id);
  }

  void clear() {
    storage.erase();
  }
}
