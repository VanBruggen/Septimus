import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:septimus/task/controllers/task_explorer.controller.dart';
import 'package:septimus/task/task.model.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class TaskEditView extends StatelessWidget {
  TaskEditView({super.key, required this.task});

  final Task task;

  final TaskExplorerController controller = Get.find();

  final _formKey = GlobalKey<FormBuilderState>();

  final _nameMaxLength = 70;
  final _placeMaxLength = 70;
  final _descriptionMaxLength = 5000;
  final _priorityOptions = [
    Priority.veryHigh,
    Priority.high,
    Priority.normal,
    Priority.low,
    Priority.veryLow
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(child: Text('Edit task details')),
        actions: [
          IconButton(
            onPressed: () {
              _formKey.currentState!.save();
              controller.setDetails(
                target: task,
                newName: _formKey.currentState!.value['name'],
                newPriority: _formKey.currentState!.value['priority'],
                newDeadline: _formKey.currentState!.value['deadline'],
                newDescription: GetUtils.isBlank(
                        _formKey.currentState!.value['description'])!
                    ? null
                    : _formKey.currentState!.value['description'],
                newPlace:
                    GetUtils.isBlank(_formKey.currentState!.value['place'])!
                        ? null
                        : _formKey.currentState!.value['place'],
              );
              Get.back();
              Get.back();
            },
            icon: const Icon(Icons.done),
          ),
        ],
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.only(
          left: 10,
          right: 10,
        ),
        child: Column(
          children: [
            FormBuilder(
              key: _formKey,
              onChanged: () {
                _formKey.currentState!.save();
              },
              child: Column(
                children: [
                  FormBuilderTextField(
                    name: 'name',
                    initialValue: task.name,
                    maxLength: _nameMaxLength,
                    decoration: const InputDecoration(
                      labelText: 'Name:',
                    ),
                  ),
                  FormBuilderDropdown(
                    name: 'priority',
                    initialValue: task.priority,
                    items: _priorityOptions
                        .map((priority) => DropdownMenuItem(
                              alignment: AlignmentDirectional.centerEnd,
                              value: priority,
                              child: Text(priority.toString()),
                            ))
                        .toList(),
                    decoration: const InputDecoration(
                      labelText: 'Priority:',
                    ),
                  ),
                  FormBuilderDateTimePicker(
                    name: 'deadline',
                    initialEntryMode: DatePickerEntryMode.calendar,
                    initialValue: task.deadline,
                    inputType: InputType.both,
                    decoration: InputDecoration(
                      labelText: 'Deadline:',
                      suffixIcon: IconButton(
                        icon: const Icon(Icons.close),
                        onPressed: () {
                          _formKey.currentState!.fields['deadline']
                              ?.didChange(null);
                        },
                      ),
                    ),
                  ),
                  FormBuilderTextField(
                    name: 'place',
                    initialValue: task.place,
                    maxLength: _placeMaxLength,
                    decoration: const InputDecoration(
                      labelText: 'Place:',
                    ),
                  ),
                  FormBuilderTextField(
                    name: 'description',
                    initialValue: task.description,
                    maxLength: _descriptionMaxLength,
                    maxLines: null,
                    keyboardType: TextInputType.multiline,
                    decoration: const InputDecoration(
                      labelText: 'Description:',
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
