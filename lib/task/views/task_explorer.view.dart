import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:septimus/task/controllers/task_explorer.controller.dart';
import 'package:septimus/task/controllers/task_move_copy.controller.dart';
import 'package:septimus/task/task.model.dart';
import 'package:septimus/task/views/task_edit.view.dart';
import 'package:septimus/task/views/task_move_copy.view.dart';

class TaskExplorerView extends StatelessWidget {
  TaskExplorerView({super.key});

  final TaskExplorerController explorerController = Get.find();

  final _newTaskTextEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        explorerController.navigateUp();
        return false;
      },
      child: GetBuilder<TaskExplorerController>(
        init: explorerController,
        builder: (_) => Scaffold(
          resizeToAvoidBottomInset: true,
          appBar: AppBar(
            leading: explorerController.currentTask.id > 0
                ? IconButton(
                    onPressed: () => explorerController.navigateToRoot(),
                    icon: const Icon(Icons.home),
                  )
                : null,
            title: explorerController.currentTask.id > 0
                ? Text(
                    'Task: ${explorerController.currentTask.name}',
                    overflow: TextOverflow.ellipsis,
                  )
                : const Text('Home'),
            actions: [
              GestureDetector(
                child: IconButton(
                  onPressed: () => showSortSubtasksByDialog(context),
                  icon: const Icon(Icons.sort),
                ),
                onLongPress: () {
                  explorerController.sort();
                },
              ),
              explorerController.currentTask.id > 0
                  ? IconButton(
                      onPressed: () => explorerController.navigateUp(),
                      icon: const Icon(Icons.arrow_upward),
                    )
                  : const SizedBox(
                      width: 48,
                    ),
              const SizedBox(width: 8),
            ],
          ),
          body: Column(
            children: [
              Expanded(
                child: CustomScrollView(slivers: <Widget>[
                  if (explorerController.currentTask.id > 0)
                    SliverList.list(children: [
                      ListTile(
                        title: const Text('Details:'),
                        titleTextStyle: Theme.of(context).textTheme.titleMedium,
                        trailing: explorerController.isDetailsSectionExpanded
                            ? Row(mainAxisSize: MainAxisSize.min, children: [
                                IconButton(
                                    onPressed: () => Get.to(
                                          () => TaskEditView(
                                              task: explorerController
                                                  .currentTask),
                                        ),
                                    icon: const Icon(Icons.edit)),
                                IconButton(
                                  onPressed: () {
                                    explorerController.hideCurrentTaskDetails();
                                  },
                                  icon:
                                      const Icon(Icons.keyboard_arrow_up_sharp),
                                ),
                              ])
                            : IconButton(
                                onPressed: () {
                                  explorerController.showCurrentTaskDetails();
                                },
                                icon:
                                    const Icon(Icons.keyboard_arrow_down_sharp),
                              ),
                      ),
                      const Divider(height: 0),
                      if (explorerController.isDetailsSectionExpanded)
                        Column(mainAxisSize: MainAxisSize.min, children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 16),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  width: 112,
                                  padding: const EdgeInsets.all(8),
                                  alignment: Alignment.centerRight,
                                  child: Text(
                                    'Name:',
                                    style: Theme.of(context)
                                        .textTheme
                                        .titleMedium!
                                        .copyWith(fontWeight: FontWeight.bold),
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    padding: const EdgeInsets.all(8),
                                    alignment: Alignment.centerLeft,
                                    child: SelectionArea(
                                      child: Text(
                                          explorerController.currentTask.name,
                                          softWrap: true,
                                          style: Theme.of(context)
                                              .textTheme
                                              .titleMedium),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                width: 112,
                                padding: const EdgeInsets.all(8),
                                alignment: Alignment.centerRight,
                                child: Text(
                                  'Priority:',
                                  style: Theme.of(context)
                                      .textTheme
                                      .titleMedium!
                                      .copyWith(fontWeight: FontWeight.bold),
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  padding: const EdgeInsets.all(8),
                                  alignment: Alignment.centerLeft,
                                  child: SelectionArea(
                                    child: Text(
                                      explorerController.currentTask.priority
                                          .toString(),
                                      style: Theme.of(context)
                                          .textTheme
                                          .titleMedium,
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                width: 112,
                                padding: const EdgeInsets.all(8),
                                alignment: Alignment.centerRight,
                                child: Text(
                                  'Deadline:',
                                  style: Theme.of(context)
                                      .textTheme
                                      .titleMedium!
                                      .copyWith(fontWeight: FontWeight.bold),
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  padding: const EdgeInsets.all(8),
                                  alignment: Alignment.centerLeft,
                                  child:
                                      explorerController.currentTask.deadline !=
                                              null
                                          ? SelectionArea(
                                              child: Text(
                                                '${explorerController.currentTask.deadline!.day}.'
                                                '${explorerController.currentTask.deadline!.month < 10 ? 0 : ''}'
                                                '${explorerController.currentTask.deadline!.month}.'
                                                '${explorerController.currentTask.deadline!.year}'
                                                ', '
                                                '${explorerController.currentTask.deadline!.hour}:'
                                                '${explorerController.currentTask.deadline!.minute < 10 ? 0 : ''}'
                                                '${explorerController.currentTask.deadline!.minute}',
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .titleMedium,
                                              ),
                                            )
                                          : Text(
                                              '--',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .titleMedium,
                                            ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                width: 112,
                                padding: const EdgeInsets.all(8),
                                alignment: Alignment.centerRight,
                                child: Text(
                                  'Place:',
                                  style: Theme.of(context)
                                      .textTheme
                                      .titleMedium!
                                      .copyWith(fontWeight: FontWeight.bold),
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  padding: const EdgeInsets.all(8),
                                  alignment: Alignment.centerLeft,
                                  child: explorerController.currentTask.place !=
                                          null
                                      ? SelectionArea(
                                          child: Text(
                                            explorerController
                                                .currentTask.place!,
                                            softWrap: true,
                                            style: Theme.of(context)
                                                .textTheme
                                                .titleMedium,
                                          ),
                                        )
                                      : Text(
                                          '--',
                                          style: Theme.of(context)
                                              .textTheme
                                              .titleMedium,
                                        ),
                                ),
                              )
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 16),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  width: 112,
                                  padding: const EdgeInsets.all(8),
                                  alignment: Alignment.centerRight,
                                  child: Text(
                                    'Description:',
                                    style: Theme.of(context)
                                        .textTheme
                                        .titleMedium!
                                        .copyWith(fontWeight: FontWeight.bold),
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    padding: const EdgeInsets.all(8),
                                    alignment: Alignment.centerLeft,
                                    child: explorerController
                                                .currentTask.description !=
                                            null
                                        ? SelectionArea(
                                            child: Text(
                                              explorerController
                                                  .currentTask.description!,
                                              softWrap: true,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .titleMedium,
                                            ),
                                          )
                                        : Text(
                                            '--',
                                            style: Theme.of(context)
                                                .textTheme
                                                .titleMedium,
                                          ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ]),
                      if (explorerController.isDetailsSectionExpanded)
                        const Divider(height: 0),
                      ListTile(
                        title: const Text('Subtasks:'),
                        titleTextStyle: Theme.of(context).textTheme.titleMedium,
                        visualDensity: VisualDensity.compact,
                      ),
                      const Divider(height: 0),
                    ]),
                  if (explorerController.currentTask.subtasksIds != null)
                    SliverPrototypeExtentList.builder(
                      itemCount:
                          explorerController.currentTask.subtasksIds!.length,
                      prototypeItem: SubtaskTile(
                        task: Task(
                          id: 900000000,
                          parentId: 800000000,
                          name: 'Title',
                          deadline: DateTime.now(),
                          description:
                              'Lorem ipsum dolor sit amet lorem ipsum dolor'
                              'sit amet lorem ipsum dolor sit amet lorem ipsum dolor'
                              'sit amet.',
                          place: 'CW148',
                        ),
                      ),
                      itemBuilder: (context, index) => SubtaskTile(
                          task:
                              explorerController.currentTask.subtasks![index]),
                    ),
                ]),
              ),
              BottomAppBar(
                color: Theme.of(context).primaryColor,
                child: IntrinsicHeight(
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: [
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(right: 8.0),
                                child: TextField(
                                  controller: _newTaskTextEditingController,
                                  maxLength: 70,
                                  maxLengthEnforcement:
                                      MaxLengthEnforcement.enforced,
                                  decoration: const InputDecoration(
                                    hintText: 'Add new task...',
                                    border: OutlineInputBorder(
                                      borderSide: BorderSide.none,
                                    ),
                                    filled: true,
                                    fillColor: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                            IconButton(
                              padding: EdgeInsets.zero,
                              onPressed: () {
                                if (GetUtils.isBlank(
                                    _newTaskTextEditingController.text)!) {
                                  _newTaskTextEditingController.clear();
                                } else {
                                  final temp =
                                      _newTaskTextEditingController.text;
                                  _newTaskTextEditingController.clear();
                                  explorerController.newSubtask(
                                      newTaskName: temp);
                                }
                              },
                              icon: const Icon(Icons.add),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            PopupMenuButton<String>(
                              tooltip: '(Un)check all subtasks',
                              icon: const Icon(Icons.done_all),
                              onSelected: (String choice) {
                                if (choice == 'check') {
                                  explorerController.checkAllSubtasks();
                                } else {
                                  explorerController.uncheckAllSubtasks();
                                }
                              },
                              itemBuilder: (BuildContext context) =>
                                  <PopupMenuEntry<String>>[
                                const PopupMenuItem<String>(
                                  value: 'check',
                                  child: Text('Check all subtasks'),
                                ),
                                const PopupMenuItem<String>(
                                  value: 'uncheck',
                                  child: Text('Uncheck all subtasks'),
                                ),
                              ],
                            ),
                            IconButton(
                                onPressed: () async {
                                  if (explorerController
                                              .currentTask.subtasksIds !=
                                          null &&
                                      explorerController.currentTask.subtasks!
                                          .any((_) => _.isChecked)) {
                                    Task? target = await Get.to(
                                      () => MoveCopyDialog(
                                        method: MoveCopyDialogMethod.move,
                                        forbiddenTasksIds: explorerController
                                            .currentTask.subtasks!
                                            .where((_) => _.isChecked)
                                            .map((checkedSubtask) =>
                                                checkedSubtask.id)
                                            .toList(),
                                      ),
                                    );
                                    if (target != null &&
                                        target.id !=
                                            explorerController.currentTask.id) {
                                      explorerController.moveCheckedSubtasks(
                                        to: target,
                                      );
                                    }
                                  }
                                },
                                icon: const Icon(Icons.east)),
                            IconButton(
                                onPressed: () async {
                                  if (explorerController
                                              .currentTask.subtasksIds !=
                                          null &&
                                      explorerController.currentTask.subtasks!
                                          .any((_) => _.isChecked)) {
                                    Task? target = await Get.to(
                                      () => MoveCopyDialog(
                                        method: MoveCopyDialogMethod.copy,
                                        forbiddenTasksIds: explorerController
                                            .currentTask.subtasks!
                                            .where((_) => _.isChecked)
                                            .map((checkedSubtask) =>
                                                checkedSubtask.id)
                                            .toList(),
                                      ),
                                    );
                                    if (target != null &&
                                        target.id !=
                                            explorerController.currentTask.id) {
                                      explorerController.copyCheckedSubtasks(
                                        to: target,
                                      );
                                    }
                                    Get.delete<TaskMoveCopyController>();
                                  }
                                },
                                icon: const Icon(Icons.copy)),
                            IconButton(
                                onPressed: () {
                                  if (explorerController
                                              .currentTask.subtasksIds !=
                                          null &&
                                      explorerController.currentTask.subtasks!
                                          .any(
                                              (subtask) => subtask.isChecked)) {
                                    showMultipleTaskDeleteDialog(context);
                                  }
                                },
                                icon: const Icon(Icons.delete)),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class SubtaskTile extends StatelessWidget {
  SubtaskTile({super.key, required this.task});

  final Task task;

  final TaskExplorerController explorerController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Flexible(
          child: Row(
            children: [
              GestureDetector(
                child: SizedBox(
                  width: 60,
                  height: 96,
                  child: Checkbox(
                    value: task.isChecked,
                    onChanged: (value) {
                      explorerController.toggleSubtaskCheck(task);
                    },
                  ),
                ),
                onTap: () {
                  explorerController.toggleSubtaskCheck(task);
                },
              ),
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    explorerController.navigateTo(id: task.id);
                  },
                  onLongPress: () {
                    showTaskActionsDialog(context, task);
                  },
                  onSecondaryTap: () {
                    showTaskActionsDialog(context, task);
                  },
                  child: Row(
                    children: [
                      Expanded(
                        child: Column(
                          children: [
                            Container(
                              height: 32,
                              color: Colors.transparent,
                              alignment: Alignment.centerLeft,
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 4),
                              child: Text(task.name,
                                  style:
                                      Theme.of(context).textTheme.titleMedium,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1),
                            ),
                            Row(
                              children: [
                                Expanded(
                                  child: Container(
                                    height: 32,
                                    color: Colors.transparent,
                                    alignment: Alignment.centerRight,
                                    padding: const EdgeInsets.only(
                                        left: 4, right: 12),
                                    child: task.deadline != null
                                        ? Row(
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              const Padding(
                                                padding:
                                                    EdgeInsets.only(right: 12),
                                                child: Icon(
                                                  Icons.calendar_today,
                                                  size: 16,
                                                ),
                                              ),
                                              Text(
                                                '${task.deadline!.day}.'
                                                '${task.deadline!.month < 10 ? 0 : ''}'
                                                '${task.deadline!.month}.'
                                                '${task.deadline!.year}'
                                                ', '
                                                '${task.deadline!.hour}:'
                                                '${task.deadline!.minute < 10 ? 0 : ''}'
                                                '${task.deadline!.minute}',
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .bodyMedium,
                                              ),
                                            ],
                                          )
                                        : null,
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Expanded(
                                  child: Container(
                                    height: 32,
                                    color: Colors.transparent,
                                    alignment: Alignment.centerRight,
                                    padding: const EdgeInsets.only(
                                        left: 4, right: 12),
                                    child: task.place != null
                                        ? Row(
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              const Padding(
                                                padding:
                                                    EdgeInsets.only(right: 8),
                                                child: Icon(
                                                  Icons.location_on,
                                                  size: 16,
                                                ),
                                              ),
                                              Text(
                                                task.place!,
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .bodyMedium,
                                                overflow: TextOverflow.ellipsis,
                                              ),
                                            ],
                                          )
                                        : null,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      if (task.subtasksIds != null)
                        Container(
                          color: Colors.transparent,
                          child: Container(
                            width: 32,
                            margin: const EdgeInsets.symmetric(horizontal: 12),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(
                                width: 1,
                                color: Colors.black,
                              ),
                            ),
                            child: Text('${task.subtasksIds!.length}'),
                          ),
                        ),
                    ],
                  ),
                ),
              ),
              GestureDetector(
                child: Container(
                  width: 32,
                  decoration: BoxDecoration(
                    color: task.priority == Priority.veryHigh
                        ? Colors.red[200]
                        : (task.priority == Priority.high
                            ? Colors.orange[200]
                            : (task.priority == Priority.normal
                                ? Colors.blue[200]
                                : (task.priority == Priority.low
                                    ? Colors.green[200]
                                    : Colors.grey[400]))),
                  ),
                ),
                onTap: () => explorerController.cycleSubtaskPriority(task),
              ),
            ],
          ),
        ),
        const Divider(height: 0),
      ],
    );
  }
}

void showTaskActionsDialog(BuildContext context, Task task) {
  final TaskExplorerController explorerController = Get.find();

  showDialog<void>(
    context: context,
    builder: (BuildContext context) => SingleChildScrollView(
      child: AlertDialog(
        title: const Center(
          child: Text('Task actions:'),
        ),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            MaterialButton(
              minWidth: double.infinity,
              height: 64,
              color: Colors.grey[200],
              onPressed: () async {
                Task? target = await Get.to(
                  () => MoveCopyDialog(
                    method: MoveCopyDialogMethod.move,
                    forbiddenTasksIds: [task.id],
                  ),
                );
                if (target != null &&
                    target.id != explorerController.currentTask.id) {
                  explorerController.moveSubtask(
                    to: target,
                    taskToMove: task,
                  );
                }
              },
              child: const Text('Move to...'),
            ),
            SizedBox.fromSize(
              size: const Size.fromHeight(8),
            ),
            MaterialButton(
              minWidth: double.infinity,
              height: 64,
              color: Colors.grey[200],
              onPressed: () async {
                Task? target = await Get.to(
                  () => MoveCopyDialog(
                    method: MoveCopyDialogMethod.copy,
                    forbiddenTasksIds: [task.id],
                  ),
                );
                if (target != null &&
                    target.id != explorerController.currentTask.id) {
                  explorerController.copySubtask(
                    to: target,
                    taskToCopy: task,
                  );
                }
              },
              child: const Text('Copy to...'),
            ),
            SizedBox.fromSize(
              size: const Size.fromHeight(8),
            ),
            MaterialButton(
              minWidth: double.infinity,
              height: 64,
              color: Colors.grey[200],
              onPressed: () {
                Get.to(
                  () => TaskEditView(task: task),
                );
              },
              child: const Text('Edit'),
            ),
            SizedBox.fromSize(
              size: const Size.fromHeight(8),
            ),
            MaterialButton(
              minWidth: double.infinity,
              height: 64,
              color: Colors.grey[200],
              onPressed: () {
                showTaskDeleteDialog(
                  context,
                  task,
                );
              },
              child: const Text('Delete'),
            ),
            SizedBox.fromSize(
              size: const Size.fromHeight(8),
            ),
            MaterialButton(
              minWidth: double.infinity,
              height: 64,
              color: Colors.red[200],
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text('Cancel'),
            ),
          ],
        ),
      ),
    ),
  );
}

void showTaskDeleteDialog(BuildContext context, Task task) {
  final TaskExplorerController explorerController = Get.find();

  showDialog<void>(
    context: context,
    builder: (BuildContext context) => AlertDialog(
      title: const Text('Delete task'),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          task.subtasksIds != null
              ? Text(
                  'Delete task \'${task.name}\' along with its ${task.subtasksIds!.length} subtasks?')
              : Text('Delete task \'${task.name}\'?'),
          Padding(
            padding: const EdgeInsets.only(top: 32),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                  child: MaterialButton(
                    height: 64,
                    color: Colors.red[200],
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text('No'),
                  ),
                ),
                Expanded(
                  child: MaterialButton(
                    height: 64,
                    color: Colors.green[200],
                    onPressed: () {
                      explorerController.deleteSubtask(task);
                      Navigator.of(context).pop();
                      Navigator.of(context).pop();
                    },
                    child: const Text('Yes'),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    ),
  );
}

void showMultipleTaskDeleteDialog(BuildContext context) {
  final TaskExplorerController explorerController = Get.find();
  final taskCount = explorerController.currentTask.subtasks!
      .where((subtask) => subtask.isChecked)
      .length;
  final cumulativeSubtaskCount = explorerController.currentTask.subtasks!
      .where((subtask) => subtask.isChecked)
      .map((checkedSubtask) => checkedSubtask.subtasksIds != null
          ? checkedSubtask.subtasksIds!.length
          : 0)
      .reduce((sum, subtasksCount) => sum + subtasksCount);

  showDialog<void>(
    context: context,
    builder: (BuildContext context) => AlertDialog(
      title: const Text('Delete checked tasks'),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
              'Delete $taskCount tasks along with their $cumulativeSubtaskCount subtasks?'),
          Padding(
            padding: const EdgeInsets.only(top: 32),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                  child: MaterialButton(
                    height: 64,
                    color: Colors.red[200],
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text('No'),
                  ),
                ),
                Expanded(
                  child: MaterialButton(
                    height: 64,
                    color: Colors.green[200],
                    onPressed: () {
                      explorerController.deleteCheckedSubtasks();
                      Navigator.of(context).pop();
                    },
                    child: const Text('Yes'),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    ),
  );
}

void showSortSubtasksByDialog(BuildContext context) {
  TaskExplorerController explorerController = Get.find();

  showDialog<void>(
    context: context,
    builder: (BuildContext context) => SingleChildScrollView(
      child: AlertDialog(
        title: const Center(child: Text('Sort subtasks by:')),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            MaterialButton(
              minWidth: double.infinity,
              height: 80,
              color: Colors.grey[200],
              onPressed: () {
                explorerController
                    .setCurrentTaskSortMode(SubtasksSortMode.byPriority);
                Navigator.of(context).pop();
              },
              child: Text(
                'Priority',
                style: Theme.of(context).textTheme.titleMedium,
              ),
            ),
            SizedBox.fromSize(
              size: const Size.fromHeight(8),
            ),
            MaterialButton(
              minWidth: double.infinity,
              height: 80,
              color: Colors.grey[200],
              onPressed: () {
                explorerController
                    .setCurrentTaskSortMode(SubtasksSortMode.byDeadline);
                Navigator.of(context).pop();
              },
              child: Text(
                'Deadline',
                style: Theme.of(context).textTheme.titleMedium,
              ),
            ),
            SizedBox.fromSize(
              size: const Size.fromHeight(8),
            ),
            MaterialButton(
              minWidth: double.infinity,
              height: 80,
              color: Colors.grey[200],
              onPressed: () {
                explorerController
                    .setCurrentTaskSortMode(SubtasksSortMode.byName);
                Navigator.of(context).pop();
              },
              child: Text(
                'Name',
                style: Theme.of(context).textTheme.titleMedium,
              ),
            ),
            SizedBox.fromSize(
              size: const Size.fromHeight(8),
            ),
            MaterialButton(
              minWidth: double.infinity,
              height: 80,
              color: Colors.grey[200],
              onPressed: () {
                explorerController
                    .setCurrentTaskSortMode(SubtasksSortMode.byPlace);
                Navigator.of(context).pop();
              },
              child: Text(
                'Place',
                style: Theme.of(context).textTheme.titleMedium,
              ),
            ),
            SizedBox.fromSize(
              size: const Size.fromHeight(8),
            ),
            MaterialButton(
              minWidth: double.infinity,
              height: 80,
              color: Colors.red[200],
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(
                'Cancel',
                style: Theme.of(context).textTheme.titleMedium,
              ),
            ),
          ],
        ),
      ),
    ),
  );
}
