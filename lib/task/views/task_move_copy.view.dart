import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:septimus/task/controllers/task_explorer.controller.dart';
import 'package:septimus/task/controllers/task_move_copy.controller.dart';
import 'package:septimus/task/task.model.dart';

enum MoveCopyDialogMethod { copy, move }

class MoveCopyDialog extends StatelessWidget {
  MoveCopyDialog(
      {super.key, required this.method, required this.forbiddenTasksIds});
  final MoveCopyDialogMethod method;
  final List<int> forbiddenTasksIds;

  final moveCopyController =
      Get.put<TaskMoveCopyController>(TaskMoveCopyController());

  List<Task>? get validSubtasks => moveCopyController.currentTask.subtasks
      ?.where((subtask) => !forbiddenTasksIds.contains(subtask.id))
      .toList();

  Text matchAppBarText(MoveCopyDialogMethod method) {
    switch (method) {
      case MoveCopyDialogMethod.move:
        return const Text('Move task(s)');
      case MoveCopyDialogMethod.copy:
        return const Text('Copy task(s)');
    }
  }

  Text matchSubmitButtonText(MoveCopyDialogMethod method) {
    switch (method) {
      case MoveCopyDialogMethod.move:
        return const Text('Move task(s) here');
      case MoveCopyDialogMethod.copy:
        return const Text('Copy task(s) here');
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        moveCopyController.navigateUp();
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          centerTitle: true,
          title: matchAppBarText(method),
        ),
        body: GetBuilder<TaskMoveCopyController>(
          init: moveCopyController,
          builder: (_) => Scaffold(
            resizeToAvoidBottomInset: true,
            appBar: AppBar(
              automaticallyImplyLeading: false,
              leading: moveCopyController.currentTask.id > 0
                  ? IconButton(
                      onPressed: () => moveCopyController.navigateToRoot(),
                      icon: const Icon(Icons.home),
                    )
                  : null,
              title: moveCopyController.currentTask.id > 0
                  ? Text(
                      'Task: ${moveCopyController.currentTask.name}',
                      overflow: TextOverflow.ellipsis,
                    )
                  : const Text('Home'),
              actions: [
                moveCopyController.currentTask.id > 0
                    ? IconButton(
                        onPressed: () => moveCopyController.navigateUp(),
                        icon: const Icon(Icons.arrow_upward),
                      )
                    : const SizedBox(
                        width: 48,
                      ),
                const SizedBox(width: 8),
              ],
            ),
            body: Column(
              children: [
                Expanded(
                  child: CustomScrollView(slivers: <Widget>[
                    if (moveCopyController.currentTask.id > 0)
                      SliverList.list(children: [
                        ListTile(
                          title: const Text('Subtasks:'),
                          titleTextStyle:
                              Theme.of(context).textTheme.titleMedium,
                          visualDensity: VisualDensity.compact,
                        ),
                        const Divider(height: 0),
                      ]),
                    if (validSubtasks != null)
                      SliverPrototypeExtentList.builder(
                        itemCount: validSubtasks!.length,
                        prototypeItem: MoveCopySubtaskTile(
                          task: Task(
                            id: 900000000,
                            parentId: 800000000,
                            name: 'Title',
                            deadline: DateTime.now(),
                            description:
                                'Lorem ipsum dolor sit amet lorem ipsum dolor'
                                'sit amet lorem ipsum dolor sit amet lorem ipsum dolor'
                                'sit amet.',
                            place: 'CW148',
                          ),
                        ),
                        itemBuilder: (context, index) =>
                            MoveCopySubtaskTile(task: validSubtasks![index]),
                      ),
                  ]),
                ),
                BottomAppBar(
                  color: Theme.of(context).primaryColor,
                  child: SizedBox.fromSize(
                    size: const Size.fromHeight(64),
                    child: Row(
                      children: [
                        Expanded(
                          child: SizedBox.expand(
                            child: MaterialButton(
                              onPressed: () {
                                Get.back();
                                // Get.delete<TaskMoveCopyController>();
                              },
                              color: Colors.red[200],
                              child: const Text('Cancel'),
                            ),
                          ),
                        ),
                        Expanded(
                          child: SizedBox.expand(
                            child: MaterialButton(
                              onPressed: () {
                                Get.back(
                                    result: moveCopyController.currentTask);
                                Get.back();
                                // Get.delete<TaskMoveCopyController>();
                              },
                              color: Colors.green[200],
                              child: matchSubmitButtonText(method),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class MoveCopySubtaskTile extends StatelessWidget {
  MoveCopySubtaskTile({super.key, required this.task});

  final Task task;

  final TaskMoveCopyController moveCopyController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Flexible(
          child: GestureDetector(
            onTap: () {
              moveCopyController.navigateTo(id: task.id);
            },
            // child: Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 12),
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      children: [
                        Container(
                          height: 32,
                          color: Colors.transparent,
                          alignment: Alignment.centerLeft,
                          padding: const EdgeInsets.symmetric(horizontal: 4),
                          child: Text(task.name,
                              style: Theme.of(context).textTheme.titleMedium,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1),
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: Container(
                                height: 32,
                                color: Colors.transparent,
                                alignment: Alignment.centerRight,
                                padding:
                                    const EdgeInsets.only(left: 4, right: 12),
                                child: task.deadline != null
                                    ? Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          const Padding(
                                            padding: EdgeInsets.only(right: 12),
                                            child: Icon(
                                              Icons.calendar_today,
                                              size: 16,
                                            ),
                                          ),
                                          Text(
                                            '${task.deadline!.day}.'
                                            '${task.deadline!.month < 10 ? 0 : ''}'
                                            '${task.deadline!.month}.'
                                            '${task.deadline!.year}'
                                            ', '
                                            '${task.deadline!.hour}:'
                                            '${task.deadline!.minute < 10 ? 0 : ''}'
                                            '${task.deadline!.minute}',
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyMedium,
                                          ),
                                        ],
                                      )
                                    : null,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: Container(
                                height: 32,
                                color: Colors.transparent,
                                alignment: Alignment.centerRight,
                                padding:
                                    const EdgeInsets.only(left: 4, right: 12),
                                child: task.place != null
                                    ? Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          const Padding(
                                            padding: EdgeInsets.only(right: 8),
                                            child: Icon(
                                              Icons.location_on,
                                              size: 16,
                                            ),
                                          ),
                                          Text(
                                            task.place!,
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyMedium,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ],
                                      )
                                    : null,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  if (task.subtasksIds != null)
                    Container(
                      color: Colors.transparent,
                      child: Container(
                        width: 32,
                        margin: const EdgeInsets.symmetric(horizontal: 12),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(
                            width: 1,
                            color: Colors.black,
                          ),
                        ),
                        child: Text('${task.subtasksIds!.length}'),
                      ),
                    ),
                  Container(
                    width: 32,
                    color: task.priority == Priority.veryHigh
                        ? Colors.red[200]
                        : (task.priority == Priority.high
                            ? Colors.orange[200]
                            : (task.priority == Priority.normal
                                ? Colors.blue[200]
                                : (task.priority == Priority.low
                                    ? Colors.green[200]
                                    : Colors.grey[400]))),
                  ),
                ],
              ),
            ),
            // ),
          ),
        ),
        const Divider(height: 0),
      ],
    );
  }
}
